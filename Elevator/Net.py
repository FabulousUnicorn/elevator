import torch, torch.nn as nn, random as rd
from torch.distributions.categorical import Categorical
import NetCoder

class Net(nn.Module):

    def __init__(self):
        
        super(Net, self).__init__()
        
        self.__possibleDecisions = []

        for i in range(-1,2):
            for j in range(-1,2):
                for k in range(-1,2):
                    self.__possibleDecisions.append((i,j,k))

        activation = nn.LeakyReLU()
        
        
        self.__common = nn.Sequential(
                                    nn.Linear(NetCoder.StateWidth, 201), activation,
                                    nn.Linear(201, 134), activation,
                                    nn.Linear(134, 67), activation
                                    )
        
        self.__value = nn.Sequential( 
                                    nn.Linear(67, 33), activation, 
                                    nn.Linear(33, 10), activation, 
                                    nn.Linear(10, 1)
                                    )
    
        self.__policy =  nn.Sequential( 
                                    nn.Linear(67, 134), activation, 
                                    nn.Linear(134, 67), activation, 
                                    nn.Linear(67,27)
                                    )

    def forward(self, x):
        
        base = self.__common(x)

        value = self.__value(base)
        value = value.squeeze(-1)
        logits = self.__policy(base)

        mask = torch.ones(27, dtype=torch.bool)

        if x[0][1] == 1/15: 
            mask[:9] = False
        
        if x[0][1] == 15/15: 
            mask[18:] = False
        
        if x[0][2] == 1/15: 
            mask[0:3] = False
            mask[9:12] = False
            mask[18:21] = False
        if x[0][2] == 15/15: 
            mask[6:9] = False
            mask[15:18] = False
            mask[24:27] = False
        if x[0][3] == 1/15: 
            mask[::3] = False
        if x[0][3] == 15/15: 
            mask[2::3] = False

        mask_value = torch.tensor(torch.finfo(logits.dtype).min, dtype=logits.dtype)
        logits = torch.where(mask, logits, mask_value)
        catMasked = Categorical(logits=logits)
        return catMasked.probs, value

        
    def DecideForAction(self, gameState):

        inTensor = torch.zeros((1, NetCoder.StateWidth), dtype=torch.float32)
        NetCoder.EncodeInTensor(inTensor, 0, gameState)

        with torch.no_grad():
            probs, _ = self(inTensor)
            probs = probs[0].data.cpu().numpy()

        return rd.choices(self.__possibleDecisions, probs)[0]

