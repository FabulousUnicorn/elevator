import time
import Office, Visualizer, Analyzer, Person

class Simulator:

    def __init__(self, visualize = False, analyze = False):

        self.__TICK_DELAY = 0.2

        self.__visualize = visualize
        self.__analyzer = Analyzer.Analyzer()
        self.__office = Office.Office(self.__analyzer, analyze = analyze)

        if self.__visualize:
            self.__visualizer = Visualizer.Visualizer()
            self.__visualizer.office = self.__office

    def ApplyMovement(self, movement):

        for i in range(-1,-4, -1):

            pos = self.__office.GetElevator(i).Move(movement[- i - 1])

            existingCall = next((t for t in self.__office.GetCalls() if t.destination == pos), None)
            if existingCall: 
                existingCall.Fulfill()
                self.__office.RemoveTask(existingCall)

            existingRide = next((t for t in self.__office.GetElevator(i).GetRides() if t.destination == pos), None)
            if existingRide: 
                existingRide.Fulfill()
                self.__office.GetElevator(i).RemoveRide(existingRide)

        if self.__visualize:
            time.sleep(self.__TICK_DELAY)
            
        self.__office.RunInterval()
        
        reward = Person.Person.served
        Person.Person.served = 0

        if self.__visualize:
            self.__visualizer.Update()

        if self.__office.GetTime() / 3600 >= self.__office.GetClosingHour(): 
            self.__office.Reset()
            return (True, reward)

        return (False, reward)


    def GetState(self):
        
        calls = [0] * 15
        rides1 = [0] * 15
        rides2 = [0] * 15
        rides3 = [0] * 15

        for call in self.__office.GetCalls():
            calls[call.destination - 1] = call.direction

        for ride in self.__office.GetElevator(-1).GetRides():
            rides1[ride.destination-1] = 1

        for ride in self.__office.GetElevator(-2).GetRides():
            rides2[ride.destination-1] = 1

        for ride in self.__office.GetElevator(-3).GetRides():
            rides3[ride.destination-1] = 1

        return [
            self.__office.GetTime(),

            self.__office.GetElevator(-1).GetPos(),
            self.__office.GetElevator(-2).GetPos(),
            self.__office.GetElevator(-3).GetPos(),

            len(self.__office.GetElevator(-1).GetPassengers()),
            len(self.__office.GetElevator(-2).GetPassengers()),
            len(self.__office.GetElevator(-3).GetPassengers()),

            calls,
            
            rides1,
            rides2,
            rides3      
            ] 

    def GetTime(self):
        return self.__office.GetTime()
