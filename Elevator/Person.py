import numpy
import Task

class Person:   
    served = 0
        
    def __init__(self, office, environment, analyzer):

        self.__office = office
        self.__environment = environment
        self.__analyzer = analyzer
        self.__destination = numpy.random.randint(2,16)
        self.__pos = 1
        
        self.__arrivalTime = 0
        self.__process = self.__environment.process(self.Routine())
        self.terminate = False


    def Routine(self):
        
        self.__arrivalTime = self.__office.GetTime() / 3600
        self.__analyzer.Push(self.__arrivalTime)

        while True:
            waitingEvent = self.__office.GetInLine(self, self.__environment.event())
            if waitingEvent is not None:
                yield waitingEvent

            elevator = self.__office.GetElevatorAtFloor(self.__pos)

            while not elevator:
                direction = 1 if self.__pos < self.__destination else -1
                yield self.__office.AddCall(Task.Call(self.__pos, direction, self.__environment.event()))

                elevator = self.__office.GetElevatorAtFloor(self.__pos)

            elevator.Enter(self)
            self.__office.GetOffLine(self)
            Person.served += 1
            self.__pos = -1

            yield elevator.AddRide(Task.Ride(self.__destination, self.__environment.event()))
            elevator.Exit(self)
            self.PushRideTime()
            Person.served += 10
            self.__pos = self.__destination

            if self.__pos == 1:
                break
            else:
                self.__destination = 1

            yield self.__environment.timeout(numpy.random.gamma(3))

        self.__office.Exit(self)  


    #----- getter methods -----

    def GetDestination(self): return self.__destination

    def GetPosition(self): return self.__pos

    def GetProcess(self): return self.__process

    def PushRideTime(self, value = 0):
        if value == 0:
            self.__analyzer.PushRide(self.__office.GetTime() / 3600 - self.__arrivalTime)
        else:
            self.__analyzer.PushRide(value)
