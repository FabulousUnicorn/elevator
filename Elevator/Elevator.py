class Elevator():
    
    def __init__(self, id):

        self.__id = id

        self.__passengers = []
        self.__pos = 1
        self.__rideList = []                  

    def AddRide(self, task):

        existing = next((t for t in self.__rideList if t.destination == task.destination), None)

        if existing:
            return existing.event
        else:
            self.__rideList.append(task)
            return task.event       

    def RemoveRide(self, task):
        self.__rideList.remove(task)

    def Enter(self, person): self.__passengers.append(person)  
    def Exit(self, person): self.__passengers.remove(person)
    def Move(self, dir): 
        self.__pos = self.__pos + dir
        if self.__pos < 1 or self.__pos > 15:
            raise Exception("Elevator position out of bounds: ", self.__pos)

        return self.__pos

    #----- getter methods -----

    def GetID(self): return self.__id
    def GetPassengers(self): return self.__passengers
    def GetPos(self): return self.__pos
    def GetRides(self): return self.__rideList
