import torch, torch.nn.functional as F, logging, os, keyboard
import Net, EpisodeEncoder, Simulator
from datetime import datetime
from torch.utils.tensorboard import SummaryWriter

logging.basicConfig(filename='ats.log', encoding='utf-8', level=logging.DEBUG)

FACTOR_POLICY = 10.0
FACTOR_ENTROPY = 2.0
EPSILON = 0.2

TRAIN_DAYS = 30
scoreBuffer = []

writer = SummaryWriter()
xLoss, xTotalScore = (0,0)

class TrainerPPO:

    def __init__(self):

        #self.__net = Net.Net()
        self.__net : Net = torch.load('ElevatorPPO.dat')
        self.__generator = EpisodeEncoder.EpisodeEncoder(self.__net)
        self.__optimizer = torch.optim.Adam(self.__net.parameters())   

    def __ApplyTrainingRoundSingle(self):
        global xLoss

        self.__generator.GenerateEpisodes()
        self.__optimizer.zero_grad()
        inputTensor, decisionTensor, destinationValue = self.__generator.GetTrainingTensors()
        
        logits, value = self.__net(inputTensor)
        lossValue = F.mse_loss(value, destinationValue)  

        advantage = destinationValue - value.detach()
        
        probs = F.softmax(logits, dim = 1)
        playedProbs = probs[range(inputTensor.size()[0]), decisionTensor]
        probRatio = playedProbs / playedProbs.detach()
        
        lClip = torch.min(probRatio * advantage, torch.clamp(probRatio, 1 - EPSILON, 1 + EPSILON) * advantage)
        lossPolicy = -lClip.mean()
        writer.add_scalar('policyLoss', lossPolicy, xLoss)

        logProbs = F.log_softmax(logits, dim = 1)
        entropy = torch.sum(logProbs * probs, dim = 1)
        lossEntropy =  - entropy.mean()
        writer.add_scalar('entropyLoss', lossEntropy, xLoss)

        totalLoss = lossValue + FACTOR_POLICY * lossPolicy + FACTOR_ENTROPY * lossEntropy
        writer.add_scalar('totalLoss', totalLoss, xLoss)
        xLoss += 1
        totalLoss.backward()           
        self.__optimizer.step()
            
        
    def __TestNet(self):
        global xTotalScore
        simulator = Simulator.Simulator(visualize = False, analyze = False)
        totalScore = 0
        
        stop = False
        while not stop:
            state = simulator.GetState()
            movement  = self.__net.DecideForAction(state)
            stop, score = simulator.ApplyMovement(movement)
            totalScore += score
             
        print("Test Score: " + str(totalScore) + " ")
        scoreBuffer.append(totalScore)
        logging.debug("Test Score: " + str(totalScore))
        writer.add_scalar('totalScore', totalScore, xTotalScore)
        xTotalScore += 1
        return totalScore  


    def TrainNetwork(self):

        while True:

            for i in range(TRAIN_DAYS):

                self.__ApplyTrainingRoundSingle()
                bar = '▮' * (i+1) + '▯' * (TRAIN_DAYS - (i+1))
                print(f'\rTraining: {bar}', end='\r')

            score = self.__TestNet()

            if score > 5000:

                torch.save(self.__net, 'ElevatorPPO.dat')
                LogAndExit()



def LogAndExit():

    logging.debug("TRAINING EXITED: " + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    logging.debug("TOTAL AVG SCORE: %s", sum(scoreBuffer) / max(1, len(scoreBuffer)))
    writer.flush()
    os._exit(1)

if __name__ == "__main__":

    keyboard.add_hotkey('esc', LogAndExit)
    logging.debug("TRAINING STARTED: " + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    trainer = TrainerPPO()
    trainer.TrainNetwork()
