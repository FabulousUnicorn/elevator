StateWidth = 67

OPENING_HOUR = 6
CLOSING_HOUR = 19
MAX_TIME = (CLOSING_HOUR - OPENING_HOUR) * 3600
MAX_CAPACITY = 5
NUM_FLOORS = 15

def EncodeInTensor(tensor, position, gameState):
     
    #time [0]
    tensor[position, 0] = gameState[0] / MAX_TIME

    #pos [1 - 3]
    for i in range(3):
        tensor[position, 1 + i] = gameState[1 + i] / NUM_FLOORS

    #load [4 - 6]
    for i in range(3):
        tensor[position, 4 + i] = gameState[4 + i] / MAX_CAPACITY

    #calls [7 - 21]
    for i in range(15):
        tensor[position, 7 + i] = (gameState[7][i] + 1) / 2

    #rides [22 - 66]
    for i in range(15):
        tensor[position, 22 + i * 3] = gameState[8][i] 
        tensor[position, 23 + i * 3] = gameState[9][i] 
        tensor[position, 24 + i * 3] = gameState[10][i]