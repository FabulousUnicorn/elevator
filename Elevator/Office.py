import numpy as np, simpy
from scipy.stats import gamma
import Elevator, Person, Task

class Office():

    def __init__(self, analyzer, analyze=False): 

        self.__TICK_TIME_SEC = 15 
        self.__OPENING_HOUR = 6
        self.__CLOSING_HOUR = 19
        self.__SPAWN_DELAY = 10
        self.__STEEPNESS = 3
        self.__MAX_CAPACITY = 5
        
        self.__analyzer = analyzer
        self.__analyze = analyze
        self.__environment = simpy.Environment()
        self.__elevator1 = Elevator.Elevator(-1)
        self.__elevator2 = Elevator.Elevator(-2)
        self.__elevator3 = Elevator.Elevator(-3)
        self.__guestList = []
        self.__time = self.__OPENING_HOUR * 3600
        self.__taskPool = []
        self.__waitingQueue = [[] for _ in range(15)]

        num_values = self.__CLOSING_HOUR - self.__OPENING_HOUR - 1
        values = np.linspace(self.__OPENING_HOUR, self.__CLOSING_HOUR, num_values)
        self.__spawnRates = gamma.pdf(values, 9, scale=1)
        self.__spawnRates = (self.__spawnRates - np.min(self.__spawnRates)) / (np.max(self.__spawnRates) - np.min(self.__spawnRates))

        self.__process = self.__environment.process(self.GuestSpawner())         


    def RunInterval(self):
        self.__environment.run(until=self.__environment.now + 1)
        self.__time += self.__TICK_TIME_SEC


    def GuestSpawner(self):
        
        while True:
            self.__guestList.append(Person.Person(self, self.__environment, self.__analyzer))
            yield self.__environment.timeout(self.__SPAWN_DELAY - self.__STEEPNESS * self.__spawnRates[self.__time // 3600 - self.__OPENING_HOUR - 1])


    def AddCall(self, task: Task.Call):
        
        existing = next((t for t in self.__taskPool if t.destination == task.destination and t.direction == task.direction), None)
        if existing:
            return existing.event
        else:
            self.__taskPool.append(task)
            return task.event          


    def RemoveTask(self, task):

        self.__taskPool.remove(task)


    def GetInLine(self, person, event):

        if len(self.__waitingQueue[person.GetPosition()-1]) == 0:
            self.__waitingQueue[person.GetPosition()-1].append(event)
            event.succeed()  
            return None
    
        self.__waitingQueue[person.GetPosition()-1].append(event)
        return event

    def GetOffLine(self, person):
        
        self.__waitingQueue[person.GetPosition()-1].remove(self.__waitingQueue[person.GetPosition()-1][0])

        if len(self.__waitingQueue[person.GetPosition()-1]) > 0:
             self.__waitingQueue[person.GetPosition()-1][0].succeed()
           
    def Exit(self, person): 

        self.__guestList.remove(person)

    def Reset(self):
        if self.__analyze:
            for p in self.__guestList:
                p.PushRideTime(100)
            self.__analyzer.ShowRideTimes()
        self.__process.interrupt()

        self.__environment = simpy.Environment()
        self.__elevator1 = Elevator.Elevator(-1)
        self.__elevator2 = Elevator.Elevator(-2)
        self.__elevator3 = Elevator.Elevator(-3)
        self.__guestList = []
        self.__time = self.__OPENING_HOUR * 3600
        self.__taskPool = []
        self.__waitingQueue = [[] for _ in range(15)]

        self.__environment.process(self.GuestSpawner()) 
        Person.Person.served = 0



    #----- getter methods -----

    def GetCalls(self): return self.__taskPool

    def GetClosingHour(self): return self.__CLOSING_HOUR

    def GetElevator(self, id):
        if id == -1: return self.__elevator1
        elif id == -2: return self.__elevator2   
        elif id == -3: return self.__elevator3  
        else: raise Exception("Invalid Elevator ID. (must be between -1 and -3)")

    def GetElevatorAtFloor(self, floor):

        if self.__elevator1.GetPos() == floor and len(self.__elevator1.GetPassengers()) < self.__MAX_CAPACITY: return self.__elevator1
        if self.__elevator2.GetPos() == floor and len(self.__elevator2.GetPassengers()) < self.__MAX_CAPACITY: return self.__elevator2       
        if self.__elevator3.GetPos() == floor and len(self.__elevator3.GetPassengers()) < self.__MAX_CAPACITY: return self.__elevator3
        else: return None

    def GetGuests(self): return self.__guestList

    def GetTime(self): return self.__time

    def GetQueueAtFloor(self, floor): return self.__waitingQueue[floor-1]





