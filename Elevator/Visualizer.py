import pygame

class Visualizer():

    def __init__(self):

        pygame.init()

        self.__SCREEN_WIDTH = 640
        self.__SCREEN_HEIGHT = 720
        self.__PADDING = 20
        self.__BACKGROUND_COLOR = (20,20,20,255)

        self.__screen = pygame.display.set_mode([self.__SCREEN_WIDTH, self.__SCREEN_HEIGHT])
        self.__areaSize = ((self.__SCREEN_WIDTH - self.__PADDING * 2) / 2, self.__SCREEN_HEIGHT - self.__PADDING)
        self.__canvas = pygame.Surface(self.__areaSize)
        self.__log = pygame.Surface(self.__areaSize)
        self.__h = self.__canvas.get_height()
        self.__w = self.__canvas.get_width()
        self.office = None
        self.__callBuffer = []


    def ShowCall(self, task):
        
        self.__callBuffer.append(task)


    def Update(self):

        self.__screen.fill((0,0,0))
        self.__canvas.fill(self.__BACKGROUND_COLOR)
        self.__log.fill(self.__BACKGROUND_COLOR)

        #draw scene   
        pygame.draw.rect(self.__canvas, (31,31,31), pygame.Rect(self.__w*.5, 0, self.__w, self.__h))
        
        for i in range(16):

            if i % 5 == 0:
                pygame.draw.line(self.__canvas,(255,255,255),(0,self.__h*i/15),(self.__w, self.__h*i/15), 3)
            else:
                pygame.draw.line(self.__canvas,(127,127,127),(0,self.__h*i/15),(self.__w, self.__h*i/15), 3)

        self.__printInfo()       

        #draw call triangles
        SHRINK = self.__h / 15 / 5
        for task in self.office.GetCalls():
            p1 = (self.__w - self.__h / 15 + SHRINK, 
                  self.__h - (task.destination - (task.direction + 1) / 2) * self.__h / 15 - SHRINK * task.direction)

            p2 = (self.__w - SHRINK, 
                  self.__h - (task.destination - (task.direction + 1) / 2) * self.__h / 15 - SHRINK * task.direction)

            p3 = (self.__w - self.__h/30, 
                  self.__h - (task.destination - (1 - task.direction/2 - 1/2)) * self.__h / 15 + SHRINK * task.direction)

            pygame.draw.polygon(self.__canvas, (255,31,31), (p1, p2, p3))

        #draw guests
        font = pygame.font.Font(None, 24)

        s = [""] * 15
        for person in self.office.GetGuests():
            if person.GetPosition() > 0: #person not in a lift
                s[person.GetPosition()-1] += "o "
        
        for i in range (15):                
            text = font.render(s[i], True, (255, 255, 255))
            self.__canvas.blit(text, (self.__w / 1.8, self.__h - self.__h / 15 * (i+1) + self.__h / 45))       

        #draw elevators
        pos = self.office.GetElevator(-1).GetPos()   
        colorTan = len(self.office.GetElevator(-1).GetPassengers()) * 40
        color = (255, 255 - colorTan, 255 - colorTan)
        pygame.draw.rect(self.__canvas, color, pygame.Rect(10, self.__h - pos * self.__h / 15, self.__h/20, self.__h/15))

        pos = self.office.GetElevator(-2).GetPos() 
        colorTan = len(self.office.GetElevator(-2).GetPassengers()) * 40
        color = (255, 255 - colorTan, 255 - colorTan)
        pygame.draw.rect(self.__canvas, color, pygame.Rect(50, self.__h - pos * self.__h / 15, self.__h/20, self.__h/15))
        
        pos = self.office.GetElevator(-3).GetPos()   
        colorTan = len(self.office.GetElevator(-3).GetPassengers()) * 40
        color = (255, 255 - colorTan, 255 - colorTan)
        pygame.draw.rect(self.__canvas, color, pygame.Rect(90, self.__h - pos * self.__h / 15, self.__h/20, self.__h/15))  

        self.__screen.blit(self.__canvas, (self.__PADDING / 2, self.__PADDING / 2))
        self.__screen.blit(self.__log, ((self.__SCREEN_WIDTH + self.__PADDING) / 2, self.__PADDING / 2))

        pygame.display.update()


    def __printInfo(self):

        font = pygame.font.Font(None, 24)

        for i in range(0,3):

            text = font.render("E" + str(i + 1), True, (255, 255, 255))
            self.__log.blit(text, (self.__PADDING, text.get_height() + self.__PADDING * (10 * i + 0)))
      
            s = "HALTS: "
            for t in self.office.GetElevator(-1 - i).GetRides():
                s += " " + str(t.destination)      
            text = font.render(s, True, (255, 255, 255))
            self.__log.blit(text, (self.__PADDING, text.get_height() + self.__PADDING * (10 * i + 3)))        
       
            text = font.render("PASSENGERS: " + str(len(self.office.GetElevator(-1 - i).GetPassengers())) + "/5", True, (255, 255, 255))
            self.__log.blit(text, (self.__PADDING, text.get_height() + self.__PADDING * (10 * i + 4)))

        time = self.office.GetTime()
        hours, minutes = divmod(time, 3600)
        minutes //= 60

        strTime = f"{hours:02d}:{minutes:02d}"
        text = font.render("Clock: " + strTime, True, (255, 255, 255))
        self.__log.blit(text, (self.__PADDING, self.__h - self.__PADDING - text.get_height()))