import matplotlib.pyplot as plt

class Analyzer:
    def __init__(self):
        self.__arrivalTimes = []
        self.__callTimes = []
        self.__rideTimes = []

    def ShowArrivalTimes(self):
        # Plotting histogram
        plt.hist(self.__arrivalTimes, bins=60)
        plt.xlabel('Arrival Time')
        plt.ylabel('Frequency')
        plt.title('Arrival Times Histogram')
        plt.grid(True)
        plt.show()

    def ShowCallTimes(self):
        # Plotting histogram
        plt.hist(self.__callTimes, bins=60)
        plt.xlabel('Waiting Time')
        plt.ylabel('Frequency')
        plt.title('Call Waiting Time Histogram')
        plt.grid(True)
        plt.show()

    def ShowRideTimes(self):
        # Plotting histogram
        plt.hist(self.__rideTimes, bins=60)
        plt.xlabel('Riding Time')
        plt.ylabel('Frequency')
        plt.title('Ride Waiting Time Histogram')
        plt.grid(True)
        plt.show()

    def Push(self, value): self.__arrivalTimes.append(value)
    def PushCall(self, value): self.__callTimes.append(value)
    def PushRide(self, value): self.__rideTimes.append(value)
