import torch
import NetCoder, Simulator

DISCOUNT_FACTOR = 0.9

class EpisodeEncoder:

    def __init__(self, decider):
        self.__simulator = Simulator.Simulator()
        self.__decider = decider
        self.__finalEpisodeBuffer = []          

    def GenerateEpisodes(self): 
 
        self.__finalEpisodeBuffer = []
        stop = False
        
        while not stop:

            startState = self.__simulator.GetState()
            movement = self.__decider.DecideForAction(startState)
            stop, score = self.__simulator.ApplyMovement(movement)
            self.__finalEpisodeBuffer.append((startState, movement, score))

    def GetTrainingTensors(self):

        batchSize = len(self.__finalEpisodeBuffer)
        stateTensor = torch.zeros((batchSize, NetCoder.StateWidth), dtype=torch.float32)
        actionTensor = torch.zeros((batchSize), dtype=torch.long)
        valueTensor = torch.zeros(batchSize, dtype=torch.float32)

        for i, (state, decision, score) in enumerate(self.__finalEpisodeBuffer):
            NetCoder.EncodeInTensor(stateTensor, i, state)
            
            #Ternary numeral system encoding
            actionTensor[i] = (
                                9 * (decision[0] + 1) + 
                                3 * (decision[1] + 1) + 
                                1 * (decision[2] + 1)
                                )
            
            valueTensor[i] = score

        return stateTensor, actionTensor, valueTensor